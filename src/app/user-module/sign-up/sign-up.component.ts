import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  registerForm!: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      // 'username': new FormControl(null, [Validators.required]),
      'email': new FormControl(null, [Validators.required]),
      'password': new FormControl(null, [Validators.required]),
      'firstName': new FormControl(null, [Validators.required]),
      'LastName': new FormControl(null, [Validators.required]),
      'telephone': new FormControl(null, [Validators.required]),
    })
  }

  form: FormGroup = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  // onSubmit(formData: FormGroup, formDirective: FormGroupDirective): void {

  //   const email = formData.value.email;
  //   const password = formData.value.password;
  //   const username = formData.value.username;
  //   // this.auth.registerUSer(email, password, username);
  //   //  formDirective.resetForm();
  //   this.registerForm.reset();
  // }

}
