import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  username!: string;
  password!: string;

  constructor(private router: Router) { }

  ngOnInit(): void {

  }


  systemLogin() {
    console.log(this.password + "--" + this.username);
    if (this.username === 'jobseeker') {
      if (this.password === '1234') {
        this.router.navigate(["/js/home"]);
      } else {
        console.log("Wrong password!")
      }
    } else {
      console.log("Wrong username!")
    }

  }

  // onSubmit(formData: FormGroup, formDirective: FormGroupDirective): void {

  //   const email = formData.value.email;
  //   const password = formData.value.password;
  //   const username = formData.value.username;
  //   // this.auth.registerUSer(email, password, username);
  //   //  formDirective.resetForm();
  //   this.registerForm.reset();
  // }

}
