import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { ResumeSearch } from 'src/app/classes/models/resumeSearch';
import { ResumeRequestDTO } from 'src/app/classes/models/searchCriteria';
import { ResumeService } from 'src/app/services/resume.service';

export interface ResumeDetail {
  id: number;
  name: string;
  skills: string[];
  experience: string;
  designation: string;
  subSector: string;
}

interface Skill {
  skillId: string;
  name: string;
}

interface EducationLevel {
  //id: string;
  educationalLevel: string;
}

interface ProfessionalQualification {
  //id: string;
  qualificationName: string;
}

/** Constants used to fill up our data base. */
const ELEMENT_DATA: ResumeDetail[] = [
  { id: 1, name: 'Sandaka Wijesinghe', skills: ['Spring boot Java8', 'Hibernate', 'AWS', 'Angular', 'React', 'NoSQL'], experience: '5', designation: 'Software Engineer', subSector: 'Software Engineering' },
  { id: 2, name: 'Madawa Dulanda', skills: ['.net core'], experience: '4', designation: 'Software Engineer', subSector: 'Software Engineering' },
  { id: 3, name: 'Udaya Salgado', skills: ['Spring boot Java8'], experience: '10', designation: 'Software Architect', subSector: 'Software Engineering' },
  { id: 4, name: 'Sandaka Wijesinghe', skills: ['Spring boot Java8'], experience: '5', designation: 'Software Engineer', subSector: 'Software Engineering' },
  { id: 5, name: 'Madawa Dulanda', skills: ['.net core'], experience: '4', designation: 'Software Engineer', subSector: 'Software Engineering' },
  { id: 6, name: 'Udaya Salgado', skills: ['Spring boot Java8'], experience: '10', designation: 'Software Architect', subSector: 'Software Engineering' },
  { id: 7, name: 'Sandaka Wijesinghe', skills: ['Spring boot Java8'], experience: '5', designation: 'Software Engineer', subSector: 'Software Engineering' },
  { id: 8, name: 'Madawa Dulanda', skills: ['.net core'], experience: '4', designation: 'Software Engineer', subSector: 'Software Engineering' },
  { id: 9, name: 'Udaya Salgado', skills: ['Spring boot Java8'], experience: '10', designation: 'Software Architect', subSector: 'Software Engineering' },
  { id: 10, name: 'Sandaka Wijesinghe', skills: ['Spring boot Java8'], experience: '5', designation: 'Software Engineer', subSector: 'Software Engineering' },
  { id: 11, name: 'Madawa Dulanda', skills: ['.net core'], experience: '4', designation: 'Software Engineer', subSector: 'Software Engineering' },
  { id: 12, name: 'Udaya Salgado', skills: ['Spring boot Java8'], experience: '10', designation: 'Software Architect', subSector: 'Software Engineering' },
]

@Component({
  selector: 'app-search-candidate',
  templateUrl: './search-candidate.component.html',
  styleUrls: ['./search-candidate.component.scss']
})
export class SearchCandidateComponent implements OnInit, AfterViewInit, OnDestroy {

  protected skills: Skill[] = [
    { skillId: '1', name: 'Microservices' },
    { skillId: '2', name: 'Jenkins' },
    { skillId: '3', name: 'Docker' },
    { skillId: '4', name: 'Kubernetes' },
    { skillId: '5', name: 'AWS' },
    { skillId: '6', name: 'NoSQL' },
    { skillId: '7', name: 'SQL' },
  ];

  protected eduLevels: EducationLevel[] = [
    { educationalLevel: 'Phd' },
    { educationalLevel: 'Masters' },
    { educationalLevel: 'Bachelors' },
    { educationalLevel: 'HND' },
    { educationalLevel: 'Diploma' },
    { educationalLevel: 'Certification' },
    { educationalLevel: 'Primary' },
  ];

  protected pqLevels: ProfessionalQualification[] = [
    { qualificationName: 'AWS CCP' },
    { qualificationName: 'AWS CSAA' },
    { qualificationName: 'AWS CDA' },
    { qualificationName: 'AWS CSAP' },
    { qualificationName: 'OCPJP' },
    { qualificationName: 'OCPWD' },
    { qualificationName: 'Docker CA' },
  ];

  public skillMultiCtrl: FormControl = new FormControl();
  public skillMultiFilterCtrl: FormControl = new FormControl();
  public filteredSkillsMulti: ReplaySubject<Skill[]> = new ReplaySubject<Skill[]>(1);

  public eduMultiCtrl: FormControl = new FormControl();
  public eduMultiFilterCtrl: FormControl = new FormControl();
  public filteredEduMulti: ReplaySubject<EducationLevel[]> = new ReplaySubject<EducationLevel[]>(1);

  public pqMultiCtrl: FormControl = new FormControl();
  public pqMultiFilterCtrl: FormControl = new FormControl();
  public filteredPqMulti: ReplaySubject<ProfessionalQualification[]> = new ReplaySubject<ProfessionalQualification[]>(1);

  @ViewChild('multiSelect', { static: true }) multiSelect!: MatSelect;
  protected _onDestroy = new Subject();


  searchCandidateForm!: FormGroup;


  displayedColumns: string[] = ['id', 'name', 'designation', 'experience', 'skills', 'actions'];
  dataSource!: MatTableDataSource<ResumeSearch>;
  resumeList: ResumeSearch[] = [];

  @ViewChild('paginator') paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private fb: FormBuilder, private resumeService: ResumeService) {
    //this.dataSource = new MatTableDataSource(ELEMENT_DATA);


  }

  ngOnInit(): void {

    this.loadResumes();
    this.initializeMultiSelectors();

    this.searchCandidateForm = this.fb.group({
      skillList: this.skillMultiCtrl,
      educationLevelList: this.eduMultiCtrl,
      pqList: this.pqMultiCtrl,
      subSectorId: new FormControl(''),
      experience: new FormControl(''),
    })




    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }

  initializeMultiSelectors() {
    this.skillMultiCtrl.setValue(this.skills[0]);
    this.filteredSkillsMulti.next(this.skills.slice());

    this.skillMultiFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSkillMulti();
      });


    this.eduMultiCtrl.setValue(this.eduLevels[0]);
    this.filteredEduMulti.next(this.eduLevels.slice());

    this.eduMultiFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEduMulti();
      });

    this.pqMultiCtrl.setValue(this.pqLevels[0]);
    this.filteredPqMulti.next(this.pqLevels.slice());

    this.pqMultiFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterPqMulti();
      });
  }

  loadResumes() {
   
    this.resumeService.getAllResumes(new ResumeRequestDTO).subscribe(data => {
      console.log(data)
      this.resumeList = data;
      this.dataSource = new MatTableDataSource(this.resumeList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }

  searchCandidates() {

  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }



  protected setInitialValue() {
    this.filteredSkillsMulti
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.multiSelect.compareWith = (a: Skill, b: Skill) => a && b && a.skillId === b.skillId;
      });
  }

  // Skill
  protected filterSkillMulti() {
    if (!this.skills) {
      return;
    }

    let search = this.skillMultiFilterCtrl.value;
    if (!search) {
      this.filteredSkillsMulti.next(this.skills.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSkillsMulti.next(
      this.skills.filter(bank => bank.name.toLowerCase().indexOf(search) > -1)
    );
  }

  // EDU level
  protected filterEduMulti() {
    if (!this.eduLevels) {
      return;
    }

    let search = this.eduMultiFilterCtrl.value;
    if (!search) {
      this.filteredEduMulti.next(this.eduLevels.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredEduMulti.next(
      this.eduLevels.filter(level => level.educationalLevel.toLowerCase().indexOf(search) > -1)
    );
  }

  // PQ
  protected filterPqMulti() {
    if (!this.pqLevels) {
      return;
    }

    let search = this.pqMultiFilterCtrl.value;
    if (!search) {
      this.filteredPqMulti.next(this.pqLevels.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredPqMulti.next(
      this.pqLevels.filter(pq => pq.qualificationName.toLowerCase().indexOf(search) > -1)
    );
  }

  // ngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  //   this.dataSource.sort = this.sort;
  // }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    // this.dataSource.filter = $event.target.value;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
