import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Education } from 'src/app/classes/models/education';
import { Experience } from 'src/app/classes/models/experience';
import { ProfessionalQualification } from 'src/app/classes/models/professional';
import { Resume } from 'src/app/classes/models/resume';
import { ResumeService } from 'src/app/services/resume.service';
const pdfMake = require('pdfmake/build/pdfmake.js');
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { Skill } from 'src/app/classes/models/skill';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-candidate-profile',
  templateUrl: './candidate-profile.component.html',
  styleUrls: ['./candidate-profile.component.scss']
})
export class CandidateProfileComponent implements OnInit {

  candidateProfileForm!: FormGroup;
  resume!: Resume;
  educationalQualificationList!: Education[];
  workExperienceList!: Experience[];
  professionalQualificationList!: ProfessionalQualification[];
  resumeSkillList!: Skill[];
  resumeId: number = 0;


  constructor(private formBuilder: FormBuilder, private resumeService: ResumeService,private activatedRoute: ActivatedRoute) {
    (pdfMake as any).vfs = pdfFonts.pdfMake.vfs;
  }

  ngOnInit(): void {

    this.activatedRoute.params.subscribe(data => {
      this.resumeId = data.id;
    });

    this.loadResumeDetails(this.resumeId);

    this.candidateProfileForm = this.formBuilder.group({
      fullName: new FormControl('', Validators.required),
      dateOfBirth: new FormControl('', Validators.required),
      emailAddress: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      nationality: new FormControl(0, Validators.required),
      civilStatus: new FormControl(0, Validators.required),
      //jobSector: new FormControl('', Validators.required),
      subSectorId: new FormControl('', Validators.required),
      jobTitle: new FormControl('', Validators.required),
      linkedInUrl: new FormControl('', Validators.required),
      description: new FormControl(''),
      imageUrl: new FormControl(''),
      //loggedInUserId: new FormControl(''),
      userId: new FormControl(1),
      // educationalQualificationList: this.formBuilder.array([]),
      // workExperienceList: this.formBuilder.array([]),
      // professionalQualificationList: this.formBuilder.array([]),
      //resumeSkillList: this.skillMultiCtrl
    });

  }

  // get educationalQualificationList(): FormArray {
  //   return this.candidateProfileForm.get('educationalQualificationList') as FormArray;
  // }

  // get workExperienceList(): FormArray {
  //   return this.candidateProfileForm.get('workExperienceList') as FormArray;
  // }

  // get professionalQualificationList(): FormArray {
  //   return this.candidateProfileForm.get('professionalQualificationList') as FormArray;
  // }


  loadResumeDetails(resumeId: any) {
    this.resumeService.resumeById(resumeId).subscribe(data => {
      this.resume = data;
      console.log(this.resume)

      this.candidateProfileForm.setValue({
        fullName: this.resume.fullName,
        dateOfBirth: this.resume.dateOfBirth,
        emailAddress: this.resume.emailAddress,
        phone: this.resume.phone,
        address: this.resume.address,
        gender: this.resume.gender,
        nationality: this.resume.nationality,
        civilStatus: this.resume.civilStatus,
        //jobSector: new FormControl('', Validators.required),
        subSectorId: this.resume.subSectorId,
        jobTitle: this.resume.jobTitle,
        linkedInUrl: this.resume.linkedInUrl,
        description: this.resume.description,
        imageUrl: this.resume.imageUrl,
        //loggedInUserId: this.resume.loggedInUserId,
        userId: this.resume.userId,
        // educationalQualificationList: this.resume.educationalQualificationList,
        // workExperienceList: this.resume.workExperienceList,
        // professionalQualificationList: this.resume.professionalQualificationList,
        // resumeSkillList: this.resume.resumeSkillList
      });

      this.educationalQualificationList = this.resume.educationalQualificationList;
      this.workExperienceList = this.resume.workExperienceList;
      this.professionalQualificationList = this.resume.professionalQualificationList;
      this.resumeSkillList = this.resume.resumeSkillList;
    })
  }




  /**
   * Write code on Method
   *
   * PDF Generate
   */
  generatePdf(action = 'open') {
    console.log(pdfMake);

    const documentDefinition = this.getDocumentDefinition();

    switch (action) {
      case 'open': pdfMake.createPdf(documentDefinition).open(); break;
      case 'print': pdfMake.createPdf(documentDefinition).print(); break;
      case 'download': pdfMake.createPdf(documentDefinition).download(); break;

      default: pdfMake.createPdf(documentDefinition).open(); break;
    }

  }

  getDocumentDefinition() {
    sessionStorage.setItem('resume', JSON.stringify(this.resume));

    return {
      content: [
        {
          text: 'RESUME',
          bold: true,
          fontSize: 20,
          alignment: 'center',
          margin: [0, 0, 0, 20]
        },
        {
          columns: [
            [{
              text: this.resume.fullName,
              style: 'name'
            },
            {
              text: this.resume.address
            },
            {
              text: 'Email : ' + this.resume.emailAddress,
            },
            {
              text: 'Contact No : ' + this.resume.phone,
            },
            {
              text: 'LinkedIn : ' + this.resume.linkedInUrl,
              link: this.resume.linkedInUrl,
              color: 'blue',
            }
            ],
            [
              this.getProfilePicObject()
            ]
          ]
        },
        {
          text: 'Experience',
          style: 'header'
        },
        this.getExperienceObject(this.resume.workExperienceList),
        {
          text: 'Education',
          style: 'header'
        },
        this.getEducationObject(this.resume.educationalQualificationList),
        {
          text: 'Professional Qualifications',
          style: 'header'
        },
        this.getPqObject(this.resume.professionalQualificationList),
        {
          text: 'Skills',
          style: 'header'
        },
        {
          columns: [
            {
              ul: [
                ...this.resume.resumeSkillList.filter((value, index) => index % 3 === 0).map(s => s.skillDescription)
              ]
            },
            {
              ul: [
                ...this.resume.resumeSkillList.filter((value, index) => index % 3 === 1).map(s => s.skillDescription)
              ]
            },
            {
              ul: [
                ...this.resume.resumeSkillList.filter((value, index) => index % 3 === 2).map(s => s.skillDescription)
              ]
            }
          ]
        },
        {
          text: 'Signature',
          style: 'sign'
        },
        {
          columns: [
            { qr: this.resume.fullName + ', Contact No : ' + this.resume.linkedInUrl, fit: 100 },
            {
              text: `(${this.resume.fullName})`,
              alignment: 'right',
            }
          ]
        }
      ],
      info: {
        title: this.resume.fullName + '_RESUME',
        author: this.resume.fullName,
        subject: 'RESUME',
        keywords: 'RESUME, ONLINE RESUME',
      },
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 20, 0, 10],
          decoration: 'underline'
        },
        name: {
          fontSize: 16,
          bold: true
        },
        jobTitle: {
          fontSize: 14,
          bold: true,
          italics: true
        },
        sign: {
          margin: [0, 50, 0, 10],
          alignment: 'right',
          italics: true
        },
        tableHeader: {
          bold: true,
        }
      }
    };
  }


  getExperienceObject(experiences: Experience[]) {

    const exs: any = [];

    experiences.forEach(experience => {
      exs.push(
        [{
          columns: [
            [{
              text: experience.position,
              style: 'jobTitle'
            },
            {
              text: 'at ' + experience.companyName,
            },
            {
              text: experience.description,
            }],
            {
              text: 'Experience : ' + 6 + ' Months',
              alignment: 'right'
            }
          ]
        }]
      );
    });

    return {
      table: {
        widths: ['*'],
        body: [
          ...exs
        ]
      }
    };
  }

  getEducationObject(educations: Education[]) {
    return {
      table: {
        widths: ['*', '*', '*'],
        body: [
          [{
            text: 'Degree',
            style: 'tableHeader'
          },
          {
            text: 'Institution/School',
            style: 'tableHeader'
          },
          {
            text: 'Grade',
            style: 'tableHeader'
          },
          ],
          ...educations.map(ed => {
            return [ed.educationalLevel + ' ' + ed.subject, ed.eduInstituteName, ed.eduGrade];
          })
        ]
      }
    };
  }

  getPqObject(qualifications: ProfessionalQualification[]) {
    return {
      table: {
        widths: ['*', '*', '*'],
        body: [
          [{
            text: 'Qualification',
            style: 'tableHeader'
          },
          {
            text: 'Institution/School',
            style: 'tableHeader'
          },
          {
            text: 'Grade',
            style: 'tableHeader'
          },
          ],
          ...qualifications.map(pq => {
            return [pq.qualificationName, pq.pqInstituteName, pq.pqGrade];
          })
        ]
      }
    };
  }

  getProfilePicObject() {
    if (this.resume.imageUrl) {
      return {
        image: this.resume.imageUrl,
        width: 75,
        alignment: 'right'
      };
    }
    return null;
  }
}
