import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CandidateProfileComponent } from './candidate-module/candidate-profile/candidate-profile.component';
import { SearchCandidateComponent } from './candidate-module/search-candidate/search-candidate.component';
import { DefaultLayoutComponent } from './layouts/default-layout/default-layout.component';
import { FullscreenLayoutComponent } from './layouts/fullscreen-layout/fullscreen-layout.component';
import { CreateResumeComponent } from './resume-module/create-resume/create-resume.component';
import { EditResumeComponent } from './resume-module/edit-resume/edit-resume.component';
import { SignInComponent } from './user-module/sign-in/sign-in.component';
import { SignUpComponent } from './user-module/sign-up/sign-up.component';
import { UserAccessComponent } from './user-module/user-access/user-access.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'js/home',
  },
  {
    path: 'js',
    component: DefaultLayoutComponent,
    children: [
      { path: 'home', component: CreateResumeComponent },
      { path: '', component: SearchCandidateComponent },
      { path: 'profile/:id', component: CandidateProfileComponent },
      { path: 'profile_edit/:id', component: EditResumeComponent },
      { path: 'user_access', component: UserAccessComponent },
    ]
  },

  {
    path: 'js',
    component: FullscreenLayoutComponent,
    children: [
      { path: 'signIn', component: SignInComponent },
      { path: 'signUp', component: SignUpComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
