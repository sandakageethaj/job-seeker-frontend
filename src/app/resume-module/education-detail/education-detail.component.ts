import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatExpansionPanel } from '@angular/material/expansion';
import { CreateResumeComponent } from '../create-resume/create-resume.component';

@Component({
  selector: 'app-education-detail',
  templateUrl: './education-detail.component.html',
  styleUrls: ['./education-detail.component.scss'],
  viewProviders: [MatExpansionPanel]
})
export class EducationDetailComponent implements OnInit {

  // @Input()
  // index!: number;

  // public unique_key!: number;
  // public parentRef!: CreateResumeComponent;

  //educationForm!: FormGroup;

  @Input() educationForm!: FormGroup;
  @Output() onEduFormGroupChange: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.educationForm = this.formBuilder.group({
      eduList: this.formBuilder.array([])
    })

    this.addEducation();
    
  }

  get eduList(): FormArray {
    return this.educationForm.get('eduList') as FormArray;
  }

  newEducation(): FormGroup {
    return this.formBuilder.group({
      educationLevel: new FormControl('', Validators.required),
      institute: new FormControl('', Validators.required),
      fromDate: new FormControl('', Validators.required),
      toDate: new FormControl('', Validators.required),
      subject: new FormControl('', Validators.required),
      grade: new FormControl('', Validators.required)

      // educationLevel: ['', Validators.required],
      // institute: ['', Validators.required],
      // fromDate: ['', Validators.required],
      // toDate: ['', Validators.required],
      // subject: ['', Validators.required],
      // grade: ['', Validators.required]
      // //education: this.formBuilder.array([])
    })
  }

  private addGroupToParent(): void {
    //this.educationForm.addControl('educationDetail',  this.educationForm);
    this.onEduFormGroupChange.emit(this.educationForm);
  }

  // private addGroupToParent(): void {
  //   this.formData.addControl('educationDetail',  new FormGroup(this.educationForm.controls));
  //   this.onFormGroupChange.emit(this.formData);
  // }

  addEducation() {
    this.eduList.push(this.newEducation());
    this.addGroupToParent()
  }

  removeEducation(eduIndex: number) {
    this.eduList.removeAt(eduIndex);
  }

  onSubmit() {
    console.log(this.educationForm.value)
  }

  // remove_me() {
  //   console.log("removing1... " + this.unique_key)
  //   this.parentRef.remove(this.unique_key)
  // }

}
