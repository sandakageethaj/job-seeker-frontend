import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatExpansionPanel } from '@angular/material/expansion';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-personal-detail',
  templateUrl: './personal-detail.component.html',
  styleUrls: ['./personal-detail.component.scss'],
  viewProviders: [MatExpansionPanel]
})
export class PersonalDetailComponent implements OnInit {

  personalDetailForm!: FormGroup;

  @Input() formData!: FormGroup;
  @Output() onFormGroupChange: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();



  selectedFiles?: FileList;
  selectedFileNames: string[] = [];

  progressInfos: any[] = [];
  message: string[] = [];

  previews: string[] = [];
  imageInfos?: Observable<any>;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.personalDetailForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      fullName: ['', Validators.required],
      dateOfBirth: ['', Validators.required],
      email: ['', Validators.required],
      telephone: ['', Validators.required],
      address: ['', Validators.required],
      gender: ['', Validators.required]
    });

    //this.addGroupToParent();
  }

  private addGroupToParent(): void {
    this.formData.addControl('personalDetail',  this.personalDetailForm);
    // this.formData.addControl('lastName',  this.personalDetailForm);
    // this.formData.addControl('fullName',  this.personalDetailForm);
    //this.onFormGroupChange.emit(this.personalDetailForm);
  }

  clearText(): void {
    //this.personalDetailForm.get('firstName').setValue('');
  }



  selectFiles(event: any): void {
    this.message = [];
    this.progressInfos = [];
    this.selectedFileNames = [];
    this.selectedFiles = event.target.files;

    this.previews = [];
    if (this.selectedFiles && this.selectedFiles[0]) {
      const numberOfFiles = this.selectedFiles.length;
      for (let i = 0; i < numberOfFiles; i++) {
        const reader = new FileReader();

        reader.onload = (e: any) => {
          console.log(e.target.result);
          this.previews.push(e.target.result);
        };

        reader.readAsDataURL(this.selectedFiles[i]);

        this.selectedFileNames.push(this.selectedFiles[i].name);
      }
    }
  }





  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
}
