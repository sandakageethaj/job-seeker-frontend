import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EducationDetailComponent } from '../education-detail/education-detail.component';
import { ExperienceDetailComponent } from '../experience-detail/experience-detail.component';
import { PersonalDetailComponent } from '../personal-detail/personal-detail.component';
import { ProfessionalQualificationComponent } from '../professional-qualification/professional-qualification.component';
import { SkillsComponent } from '../skills/skills.component';

@Component({
  selector: 'app-edit-resume',
  templateUrl: './edit-resume.component.html',
  styleUrls: ['./edit-resume.component.scss']
})
export class EditResumeComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openPersonalDetailDialog() {
    const dialogRef = this.dialog.open(PersonalDetailComponent, {
      maxWidth: '100vw',
      maxHeight: '100vh',
      height: '80%',
      width: '50%',
      panelClass: 'full-screen-modal'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  openEducationDetailDialog() {
    const dialogRef = this.dialog.open(EducationDetailComponent, {
      maxWidth: '100vw',
      maxHeight: '100vh',
      height: '80%',
      width: '50%',
      panelClass: 'full-screen-modal'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  openExperienceDetailDialog() {
    const dialogRef = this.dialog.open(ExperienceDetailComponent, {
      maxWidth: '100vw',
      maxHeight: '100vh',
      height: '80%',
      width: '50%',
      panelClass: 'full-screen-modal'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  openPqDetailDialog() {
    const dialogRef = this.dialog.open(ProfessionalQualificationComponent, {
      maxWidth: '100vw',
      maxHeight: '100vh',
      height: '70%',
      width: '50%',
      panelClass: 'full-screen-modal'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  openSkillsDetailDialog() {
    const dialogRef = this.dialog.open(SkillsComponent, {
      maxWidth: '100vw',
      maxHeight: '100vh',
      height: '40%',
      width: '40%',
      panelClass: 'full-screen-modal'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}


