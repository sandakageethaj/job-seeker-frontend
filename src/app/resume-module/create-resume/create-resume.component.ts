import { AfterViewInit, Component, ComponentFactoryResolver, ComponentRef, OnDestroy, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { Resume } from 'src/app/classes/models/resume';
const pdfMake = require('pdfmake/build/pdfmake.js');
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { Experience } from 'src/app/classes/models/experience';
import { Education } from 'src/app/classes/models/education';
import { ProfessionalQualification } from 'src/app/classes/models/professional';
import { Skill } from 'src/app/classes/models/skill';
import { ResumeService } from 'src/app/services/resume.service';
import { ToastrService } from 'ngx-toastr';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SkillService } from 'src/app/services/skill.service';
import { SectorService } from 'src/app/services/sector.service';
import { Sector, SubSector } from 'src/app/classes/models/sector';


// interface Skill {
//   id: string;
//   name: string;
// }

@Component({
  selector: 'app-create-resume',
  templateUrl: './create-resume.component.html',
  styleUrls: ['./create-resume.component.scss']
})
export class CreateResumeComponent implements OnInit, AfterViewInit, OnDestroy {

  // protected skills: Skill[] = [
  //   { skillId: '1', skillDescription: 'Microservices' },
  //   { skillId: '2', skillDescription: 'Jenkins' },
  //   { skillId: '3', skillDescription: 'Docker' },
  //   { skillId: '4', skillDescription: 'Kubernetes' },
  //   { skillId: '5', skillDescription: 'AWS' },
  //   { skillId: '6', skillDescription: 'NoSQL' },
  //   { skillId: '7', skillDescription: 'SQL' },
  // ];

  public skillMultiCtrl: FormControl = new FormControl();
  public skillMultiFilterCtrl: FormControl = new FormControl();
  public filteredSkillsMulti: ReplaySubject<Skill[]> = new ReplaySubject<Skill[]>(1);
  @ViewChild('multiSelect', { static: true }) multiSelect!: MatSelect;
  protected _onDestroy = new Subject();

  disabled: boolean = false;

  createResumeParentForm!: FormGroup;

  selectedFiles?: FileList;
  //selectedFileNames: string[] = [];

  progressInfos: any[] = [];
  message: string[] = [];

  previews: string[] = [];
  imageInfos?: Observable<any>;

  resume: Resume = new Resume();
  skillsList: Skill[] = [];
  sectorsList: Sector[] = [];
  subSectorsList: SubSector[] = [];


  constructor(private fb: FormBuilder, private resumeService: ResumeService, private skillService: SkillService, private sectorService: SectorService, private toastr: ToastrService, private _snackBar: MatSnackBar) {
    //this.createForm();
    (pdfMake as any).vfs = pdfFonts.pdfMake.vfs;
  }

  ngOnInit(): void {

    this.loadSkills();
    this.loadSctors();
    this.skillMultiCtrl.setValue('');
    this.filteredSkillsMulti.next(this.skillsList.slice());

    this.skillMultiFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSkillMulti();
      });

    this.createResumeParentForm = this.fb.group({
      //firstName: new FormControl('', Validators.required),
      //lastName: new FormControl('', Validators.required),
      fullName: new FormControl('', Validators.required),
      dateOfBirth: new FormControl('', Validators.required),
      emailAddress: new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      phone: new FormControl('', [Validators.required, Validators.pattern('[- +()0-9]+')]),
      address: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      nationality: new FormControl(0, Validators.required),
      civilStatus: new FormControl(0, Validators.required),
      //jobSector: new FormControl('', Validators.required),
      subSectorId: new FormControl('', Validators.required),
      jobTitle: new FormControl('', Validators.required),
      linkedInUrl: new FormControl('', Validators.required),
      description: new FormControl(''),
      imageUrl: new FormControl(''),
      loggedInUserId: new FormControl(''),
      userId: new FormControl(1),
      educationalQualificationList: this.fb.array([]),
      workExperienceList: this.fb.array([]),
      professionalQualificationList: this.fb.array([]),
      resumeSkillList: this.skillMultiCtrl
    });

    this.addEducation();
    this.addExperience();
    this.addQualification();
  }


  get educationalQualificationList(): FormArray {
    return this.createResumeParentForm.get('educationalQualificationList') as FormArray;
  }

  get workExperienceList(): FormArray {
    return this.createResumeParentForm.get('workExperienceList') as FormArray;
  }

  get professionalQualificationList(): FormArray {
    return this.createResumeParentForm.get('professionalQualificationList') as FormArray;
  }

  // get skillsList(): FormArray {
  //   return this.createResumeParentForm.get('skillsList') as FormArray;
  // }

  loadSkills() {
    this.skillService.getAllSkills().subscribe(data => {
      this.skillsList = data;
      //console.log(this.skillsList)
    })
  }

  loadSctors() {
    this.sectorService.getAllSectors().subscribe(data => {
      this.sectorsList = data;
      console.log(this.sectorsList)
    })
  }

  loadSubSectors(sectorId: any) {
    this.sectorService.getSubsectorsBySectorId(sectorId).subscribe(data => {
      this.subSectorsList = data;
      //console.log(this.skillsList)
    })
  }

  addEducation() {
    this.educationalQualificationList.push(this.newEducation());
  }

  addExperience() {
    this.workExperienceList.push(this.newExperience());
  }

  addQualification() {
    this.professionalQualificationList.push(this.newQualification());
  }

  newEducation(): FormGroup {
    return this.fb.group({
      educationalLevel: new FormControl('', Validators.required),
      eduInstituteName: new FormControl('', Validators.required),
      eduFromYear: new FormControl('', Validators.required),
      eduToYear: new FormControl('', Validators.required),
      subject: new FormControl('', Validators.required),
      eduGrade: new FormControl('', Validators.required)
    })
  }

  newExperience(): FormGroup {
    return this.fb.group({
      companyName: new FormControl(''),
      position: new FormControl(''),
      currentJob: new FormControl(false),
      startDate: new FormControl(''),
      endDate: new FormControl(''),
      description: new FormControl('')
    })
  }

  newQualification(): FormGroup {
    return this.fb.group({
      qualificationName: new FormControl(''),
      pqInstituteName: new FormControl(''),
      pqGrade: new FormControl('')
    })
  }

  removeEducation(eduIndex: number) {
    this.educationalQualificationList.removeAt(eduIndex);
  }

  removeExperience(expIndex: number) {
    this.workExperienceList.removeAt(expIndex);
  }

  removeQualification(pqIndex: number) {
    this.professionalQualificationList.removeAt(pqIndex);
  }

  selectFiles(event: any): void {
    this.message = [];
    this.progressInfos = [];
    //this.selectedFileNames = [];
    this.selectedFiles = event.target.files;

    //this.fileChanged(event);

    this.previews = [];
    if (this.selectedFiles && this.selectedFiles[0]) {
      const numberOfFiles = this.selectedFiles.length;
      for (let i = 0; i < numberOfFiles; i++) {
        const reader = new FileReader();

        reader.onload = (e: any) => {
          console.log(e.target.result);
          this.previews.push(e.target.result);
        };

        reader.readAsDataURL(this.selectedFiles[i]);

        //this.selectedFileNames.push(this.selectedFiles[i].name);
      }
    }

    // const file = event.target.files[0];
    // this.getBase64(file);
  }

  submitForm(resume: Resume): void {
    console.log(resume)
    this.resume = resume;
    this.resume.loggedInUserId = 'sandaka';
    this.resume.userId = 1;
    //this.showSuccessSnackbar(this.resume.loggedInUserId, 'close', '4000')
    this.resumeService.saveResume(this.resume).subscribe(data => {
      console.log(data), (error: any) => console.log(error)
      if (data) {
        this.showSuccessSnackbar("Your resume has been created successfully!", 'close', '4000')
        this.generatePdf('download');
      } else {
        this.showError();
      }
    });
  }

  showSuccessSnackbar(content: any, action: any, duration: any) {
    let sb = this._snackBar.open(content, action, {
      duration: duration,
      verticalPosition: "bottom", // Allowed values are  'top' | 'bottom'
      horizontalPosition: "end", // Allowed values are 'start' | 'center' | 'end' | 'left' | 'right'
      panelClass: ["success-style"]
    });
    sb.onAction().subscribe(() => {
      sb.dismiss();
    });
  }

  showErrorSnackbar(content: any, action: any, duration: any) {
    let sb = this._snackBar.open(content, action, {
      duration: duration,
      verticalPosition: "bottom", // Allowed values are  'top' | 'bottom'
      horizontalPosition: "end", // Allowed values are 'start' | 'center' | 'end' | 'left' | 'right'
      panelClass: ["error-style"]
    });
    sb.onAction().subscribe(() => {
      sb.dismiss();
    });
  }


  /**
   * Write code on Method
   *
   * PDF Generate
   */
  generatePdf(action = 'open') {
    console.log(pdfMake);
    const documentDefinition = this.getDocumentDefinition();

    switch (action) {
      case 'open': pdfMake.createPdf(documentDefinition).open(); break;
      case 'print': pdfMake.createPdf(documentDefinition).print(); break;
      case 'download': pdfMake.createPdf(documentDefinition).download(); break;

      default: pdfMake.createPdf(documentDefinition).open(); break;
    }

  }

  getDocumentDefinition() {
    sessionStorage.setItem('resume', JSON.stringify(this.resume));

    return {
      content: [
        {
          text: 'RESUME',
          bold: true,
          fontSize: 20,
          alignment: 'center',
          margin: [0, 0, 0, 20]
        },
        {
          columns: [
            [{
              text: this.resume.fullName,
              style: 'name'
            },
            {
              text: this.resume.address
            },
            {
              text: 'Email : ' + this.resume.emailAddress,
            },
            {
              text: 'Contact No : ' + this.resume.phone,
            },
            {
              text: 'LinkedIn : ' + this.resume.linkedInUrl,
              link: this.resume.linkedInUrl,
              color: 'blue',
            }
            ],
            [
              this.getProfilePicObject()
            ]
          ]
        },
        {
          text: 'Experience',
          style: 'header'
        },
        this.getExperienceObject(this.resume.workExperienceList),
        {
          text: 'Education',
          style: 'header'
        },
        this.getEducationObject(this.resume.educationalQualificationList),
        {
          text: 'Professional Qualifications',
          style: 'header'
        },
        this.getPqObject(this.resume.professionalQualificationList),
        {
          text: 'Skills',
          style: 'header'
        },
        {
          columns: [
            {
              ul: [
                ...this.resume.resumeSkillList.filter((value, index) => index % 3 === 0).map(s => s.skillDescription)
              ]
            },
            {
              ul: [
                ...this.resume.resumeSkillList.filter((value, index) => index % 3 === 1).map(s => s.skillDescription)
              ]
            },
            {
              ul: [
                ...this.resume.resumeSkillList.filter((value, index) => index % 3 === 2).map(s => s.skillDescription)
              ]
            }
          ]
        },
        {
          text: 'Signature',
          style: 'sign'
        },
        {
          columns: [
            { qr: this.resume.fullName + ', Contact No : ' + this.resume.linkedInUrl, fit: 100 },
            {
              text: `(${this.resume.fullName})`,
              alignment: 'right',
            }
          ]
        }
      ],
      info: {
        title: this.resume.fullName + '_RESUME',
        author: this.resume.fullName,
        subject: 'RESUME',
        keywords: 'RESUME, ONLINE RESUME',
      },
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          margin: [0, 20, 0, 10],
          decoration: 'underline'
        },
        name: {
          fontSize: 16,
          bold: true
        },
        jobTitle: {
          fontSize: 14,
          bold: true,
          italics: true
        },
        sign: {
          margin: [0, 50, 0, 10],
          alignment: 'right',
          italics: true
        },
        tableHeader: {
          bold: true,
        }
      }
    };
  }


  getExperienceObject(experiences: Experience[]) {

    const exs: any = [];

    experiences.forEach(experience => {
      exs.push(
        [{
          columns: [
            [{
              text: experience.position,
              style: 'jobTitle'
            },
            {
              text: 'at ' + experience.companyName,
            },
            {
              text: experience.description,
            }],
            {
              text: 'Experience : ' + 6 + ' Months',
              alignment: 'right'
            }
          ]
        }]
      );
    });

    return {
      table: {
        widths: ['*'],
        body: [
          ...exs
        ]
      }
    };
  }

  getEducationObject(educations: Education[]) {
    return {
      table: {
        widths: ['*', '*', '*'],
        body: [
          [{
            text: 'Degree',
            style: 'tableHeader'
          },
          {
            text: 'Institution/School',
            style: 'tableHeader'
          },
          {
            text: 'Grade',
            style: 'tableHeader'
          },
          ],
          ...educations.map(ed => {
            return [ed.educationalLevel + ' ' + ed.subject, ed.eduInstituteName, ed.eduGrade];
          })
        ]
      }
    };
  }

  getPqObject(qualifications: ProfessionalQualification[]) {
    return {
      table: {
        widths: ['*', '*', '*'],
        body: [
          [{
            text: 'Qualification',
            style: 'tableHeader'
          },
          {
            text: 'Institution/School',
            style: 'tableHeader'
          },
          {
            text: 'Grade',
            style: 'tableHeader'
          },
          ],
          ...qualifications.map(pq => {
            return [pq.qualificationName, pq.pqInstituteName, pq.pqGrade];
          })
        ]
      }
    };
  }

  getProfilePicObject() {
    if (this.resume.imageUrl) {
      return {
        image: this.resume.imageUrl,
        width: 75,
        alignment: 'right'
      };
    }
    return null;
  }

  fileChanged(e: any) {
    console.log("file changed... " + e)
    const file = e.target.files[0];
    this.getBase64(file);
  }

  getBase64(file: any) {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      console.log(reader.result);
      this.resume.imageUrl = reader.result as string;
    };
    reader.onerror = (error) => {
      console.log('Error: ', error);
    };
  }

  /**
   * Write code on Method
   *
   * method logical code
   */
  ngAfterViewInit() {
    this.setInitialValue();
  }

  /**
   * Write code on Method
   *
   * method logical code
   */
  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  /**
   * Write code on Method
   *
   * method logical code
   */
  protected setInitialValue() {
    this.filteredSkillsMulti
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.multiSelect.compareWith = (a: Skill, b: Skill) => a && b && a.skillId === b.skillId;
      });
  }

  /**
   * Write code on Method
   *
   * method logical code
   */
  protected filterSkillMulti() {
    if (!this.skillsList) {
      return;
    }

    let search = this.skillMultiFilterCtrl.value;
    if (!search) {
      this.filteredSkillsMulti.next(this.skillsList.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredSkillsMulti.next(
      this.skillsList.filter(bank => bank.skillDescription.toLowerCase().indexOf(search) > -1)
    );
  }


  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }


  // Toastr
  showSuccess() {
    console.log("msggg")
    this.toastr.success('New Workout Added', 'Successfully!',
      { timeOut: 3000 });;
  }
  showError() {
    this.toastr.error('Something Went Wrong!', 'Please check again',
      { timeOut: 4000 });
  }
}
