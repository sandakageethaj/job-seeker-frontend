import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatExpansionPanel } from '@angular/material/expansion';

@Component({
  selector: 'app-experience-detail',
  templateUrl: './experience-detail.component.html',
  styleUrls: ['./experience-detail.component.scss'],
  viewProviders: [MatExpansionPanel]
})
export class ExperienceDetailComponent implements OnInit {

  disabled: boolean = false;

  @Input() experienceForm!: FormGroup;
  @Output() onExpFormGroupChange: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.experienceForm = this.formBuilder.group({
      expList: this.formBuilder.array([])
    })

    this.addExperience();
  }

  get expList(): FormArray {
    return this.experienceForm.get('expList') as FormArray;
  }

  newExperience(): FormGroup {
    return this.formBuilder.group({
      companyName: new FormControl(''),
      designation: new FormControl(''),
      isCurrentJob: new FormControl(false),
      fromDate: new FormControl(''),
      toDate: new FormControl(''),
      description: new FormControl('')
      //education: this.formBuilder.array([])
    })
  }

  private addGroupToParent(): void {
    //this.educationForm.addControl('educationDetail',  this.educationForm);
    this.onExpFormGroupChange.emit(this.experienceForm);
  }

  addExperience() {
    this.expList.push(this.newExperience());
    //this.addGroupToParent()
  }

  removeExperience(expIndex: number) {
    this.expList.removeAt(expIndex);
  }

  onSubmit() {
    console.log(this.experienceForm.value)
  }

}
