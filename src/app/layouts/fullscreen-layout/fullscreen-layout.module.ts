import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FullscreenLayoutComponent } from './fullscreen-layout.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
//import { SignInComponent } from 'src/app/user-module/sign-in/sign-in.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
//import { SignUpComponent } from 'src/app/user-module/sign-up/sign-up.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { UserModuleModule } from 'src/app/user-module/user-module.module';




@NgModule({
  declarations: [
    FullscreenLayoutComponent,
    //SignInComponent,
    //SignUpComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatExpansionModule,
    MatListModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    UserModuleModule
  ]
})
export class FullscreenLayoutModule { }
