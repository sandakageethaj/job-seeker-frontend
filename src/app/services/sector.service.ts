import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Sector } from '../classes/models/sector';

@Injectable({
  providedIn: 'root'
})
export class SectorService {

  baseUrl: string = 'http://localhost:8100/api/sector/v1/'

  constructor(private http: HttpClient) { }

  getAllSectors(): Observable<Sector[]> {
    return this.http.get<Sector[]>(this.baseUrl + 'get_all_sectors');
  }

  getSubsectorsBySectorId(sectorId: number): Observable<any> {
    return this.http.get(this.baseUrl + 'get_all_subSector_by_sectorId?id=' + sectorId);
  }
}
