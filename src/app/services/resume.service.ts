import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Resume } from '../classes/models/resume';
import { ResumeRequestDTO, SearchCriteria } from '../classes/models/searchCriteria';

@Injectable({
  providedIn: 'root'
})
export class ResumeService {

  baseUrl: string = 'http://localhost:8100/api/resume/v1/'

  constructor(private http: HttpClient) { }

  saveResume(resume: Resume): Observable<Object> {
    console.log("service is working...")
    return this.http.post(this.baseUrl + 'create_resume', resume);
  }

  resumeById(id: any): Observable<any> {
    return this.http.get(this.baseUrl + 'resume_by_id?id=' + id);
  }

  getAllResumes(obj: ResumeRequestDTO): Observable<any> {
    return this.http.post(this.baseUrl + 'resumes_by_criteria', obj);
  }
}
