import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  baseUrl: string = 'http://localhost:8100/api/skill/v1/'

  constructor(private http: HttpClient) { }

  getAllSkills(): Observable<any> {
    return this.http.get(this.baseUrl + 'get_all_skills');
  }
}
