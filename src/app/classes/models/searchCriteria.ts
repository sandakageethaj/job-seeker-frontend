import { Education } from "./education";
import { ProfessionalQualification } from "./professional";
import { Skill } from "./skill";

export class SearchCriteria{
    subSectorId: any;
    experience: any;
    skillsList: Skill[] = [];
    pqList: ProfessionalQualification[] = [];
    educationLevelList: Education[] = [];

}

export class ResumeRequestDTO{
    skillsIdList: number[] = [];
    experience: any;
    subSectorId: number = 0;
    educationalLevel: string = '';
    qualificationName: string = '';
    
}