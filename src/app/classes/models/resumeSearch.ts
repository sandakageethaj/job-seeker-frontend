export class ResumeSearch{
    resumeId: any;
    fullName: string = '';
    subSectorName: string = '';
    experience: any;
    skills: string = '';
}