import { ContactInfo } from "./contactInfo";
import { Education } from "./education";
import { Experience } from "./experience";
import { ProfessionalQualification } from "./professional";
import { Skill } from "./skill";

export class Resume {
    loggedInUserId: string = '';
    userId: number = 0;
    fullName: string = '';
    dateOfBirth: string = '';
    emailAddress: string = '';
    phone: string = '';
    address: string = '';
    gender: string = '';
    nationality: string = '';
    civilStatus: number = 0;
    subSectorId: number = 0;
    jobTitle: string = '';
    linkedInUrl: string = '';
    imageUrl: string = '';
    description: string = '';
    educationalQualificationList: Education[] = [];
    workExperienceList: Experience[] = [];
    professionalQualificationList: ProfessionalQualification[] = [];
    resumeSkillList: Skill[] = [];
    //contactInfoList: ContactInfo[] = [];
}