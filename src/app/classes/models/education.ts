export class Education {
    //loggedInUserId: string = '1';
    recordStatus: number = 1;
    educationalLevel: string = '';
    eduInstituteName: string = '';
    eduFromYear: number = 2017;
    eduToYear: number = 2021;
    subject: string = '';
    eduGrade: string = '';
}