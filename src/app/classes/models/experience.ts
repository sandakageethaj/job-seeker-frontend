export class Experience {
    //loggedInUserId: string = '1';
    companyName: string = '';
    position: string = '';
    currentJob: boolean = false;
    startDate: string = '';
    endDate: string = '';
    description: string = '';
}